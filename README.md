# rancher-server

## Components

- [RKE](https://rancher.com/docs/rke/latest/en/config-options/)
- [NSX-T](https://docs.vmware.com/en/VMware-NSX-T-Data-Center/index.html)
- [vSphere](https://docs.vmware.com/en/VMware-vSphere/index.html)
- [vSphere CCM](https://github.com/kubernetes/cloud-provider-vsphere) (Set cluster as external cloud in kubelet)
- [vSphere CSI](https://github.com/kubernetes-sigs/vsphere-csi-driver) (Set for default vsan policy)
- [Cloud-Init vSphere Guest Info](https://github.com/vmware/cloud-init-vmware-guestinfo)
- [Helm](https://helm.sh/)
- [Rancher](https://rancher.com/docs/rancher/v2.x/en/)
- [Cert Manager](https://cert-manager.io/)

## How to

Environment Variables:
- AWS_ACCESS_KEY_ID="accesskeyid"
- AWS_SECRET_ACCESS_KEY="secretaccesskey"
- AWS_DEFAULT_REGION="s3region"
- AWS_S3_ENDPOINT="s3 endpoint"
- TF_VAR_vsphere_password="vsphere-password"
- TF_VAR_rancher_password="password to set rancher server"
- TF_VAR_ssh_private_key="path/to/private/key"

10.0.170.3