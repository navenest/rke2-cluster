# Rancher
rancher_count_master = 3
rancher_count_worker = 0
rancher_hostname_master = "rke2"
rancher_url = "rancher.pineapplenest.com"
rancher_api_url = "rancher.hawaiian.islands.io"
cluster_name = "rke2"
rke2_info = [
    { "ip"="192.168.251.201" },
    { "ip"="192.168.251.202" },
    { "ip"="192.168.251.203" }
]


# vShpere
vsphere_user = "gitlab@hawaiian.islands.io"
vsphere_vcenter = "vcenter.hawaiian.islands.io"
vsphere_datacenter = "nest"
vsphere_datastore = "vsanDatastore"
vsphere_vm_compute_cluster = "islands"
vsphere_resource_pool = "Resources"
vsphere_network = "dpg-100"
vsphere_virtual_machine_template = "ubuntu-20.04"
vsphere_folder = "kubernetes"

# NSX
nsx_host = "nsx.hawaiian.islands.io"
nsx_user = "gitlab@hawaiian.islands.io"
nsxt_dhcp_servers = ["172.29.0.1/30"]
nsxt_segment_subnet = "10.1.110.254/24"
nsxt_segment_dhcp_subnets = ["10.1.110.1-10.1.110.127"]
dns_servers = ["192.168.251.145"]
kube_api_ip = "10.0.170.3"

# Helm
metallb_subnet = "192.168.251.204-192.168.251.209"
metallb_enabled = true
ingress_nginx_enabled = true
ingress_ip = "192.168.251.204"