terraform {
  required_providers {
    vsphere = {
      source = "hashicorp/vsphere"
      version = "1.24.2"
    }
    rke = {
      source = "rancher/rke"
      version = "1.1.4"
    }
    rancher2 = {
      source = "rancher/rancher2"
      version = "1.10.3"
    }
    nsxt = {
      source = "vmware/nsxt"
      version = "3.0.1"
    }
    helm = {
      source = "hashicorp/helm"
      version = "1.3.2"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "1.13.2"
    }
    local = {
      source = "hashicorp/local"
      version = "2.0.0"
    }
  }
  backend "s3" {
    bucket = "terraform"
    key = "terraform.tfstate"
    force_path_style = true
    skip_credentials_validation = true
    skip_metadata_api_check = true
    skip_region_validation = true
  }
}