variable "rancher_count_master" {
  description = "Number of Rancher Servers"
  default     = 1
}

variable "rancher_count_worker" {
  description = "Number of Rancher Servers"
  default     = 2
}

variable "rancher_ip_master" {
  description = "Rancher Master IP Addresses"
  default     = "10.0.110.21"
}

variable "rancher_ip_worker" {
  description = "Rancher Worker IP Addresses"
  default     = "10.0.110.31,10.0.110.32"
}

variable "rke2_info" {
  default    =  [
    { "ip"="192.168.251.201" },
    { "ip"="192.168.251.202" },
    { "ip"="192.168.251.203" }
]

}

variable rancher_url {
  default = "rancher.pineapplenest.com"
}

variable rke2_cluster_secret {
  description = "rke2 join cluster secret"
}

variable "rancher_netmask" {
  description = "Rancher Cluster IP Netmask Digit"
  default     = "24"

}

variable "rancher_hostname_master" {
  description = "Rancher Master Hostname"
  default     = "honolulu-rke-m"
}

variable "rancher_hostname_worker" {
  description = "Rancher Worker Hostname"
  default     = "honolulu-rke-w"
}


variable "rancher_gateway" {
  description = "Rancher Cluster Gateway"
  default     = "10.0.110.254"
}

variable "dns_servers" {
  description = "DNS for Rancher Clusters"
  default     = ["192.168.251.145"]
}

variable "rancher_dns_search" {
  description = "DNS Search Domain"
  default     = "hawaiian.islands"
}

variable "vsphere_user" {
  description = "vSphere User Name"
  default     = "gitlab@vsphere.local"
}

variable "vsphere_password" {
  description = "vSphere Password"
}

variable "vsphere_vcenter" {
  description = "vCenter to connect to"
  default = "honolulu-vc.hawaiian.islands"
}

variable "vsphere_datacenter" {
  description = "vsphere datacenter"
  default = "nest"
}

variable "vsphere_datastore" {
  description = "vsphere datastore"
  default     = "vsanDatastore"
}

variable "vsphere_vm_compute_cluster" {
  description = "vsphere Computer Cluster"
  default = "islands"
}

variable "vsphere_resource_pool" {
  description = "Resource Pool"
  default     = "Resources"
}

variable "vsphere_network" {
  description = "Network"
  default     = "Trusted-SW"
}

variable "vsphere_pod_network" {
  description = "Network"
  default     = "LS1"
}

variable "vsphere_virtual_machine_template" {
  description = "Virtual Machine to Clone"
  default     = "ubuntu-20.04"
}

variable "vsphere_folder" {
  description = "Path to Folder location"
  default     = "kubernetes"
}

variable "rancher_api_url" {
  description = "Rancher Server API Endpoint"
  default     = "https://honolulu-rancher.hawaiian.islands/v3"
}

variable "rancher_password" {
  description = "Password to be set for the rancher server"
}

# variable "rancher_api_token" {
#   description = "Rancher Server API Token"
# }

variable "cluster_name" {
  description = "Rancher Cluster Name"
  default     = "honolulu-rke"
}

variable "kubernetes_version" {
  description = "Kubernetes Version"
  default     = "v1.13.10-rancher1-2"
}

# variable "registry_user" {
#   description = "User for Registry"
#   default     = "admin"
# }

# variable "registry_url" {
#   description = "Registry URL"
#   default     = "dclimagereg1.wdw.disney.com:9083"
# }

# variable "registry_pass" {
#   description = "Password for Registry"
# }

variable "nsx_host" {
  description = "NSX Manager URL"
  default     = "nsx.hawaiian.islands.io"
}

variable "nsx_user" {
  description = "NSX User"
  default     = "admin"
}

# variable "nsx_password" {
#   description = "NSX Password"
# }

variable nsxt_edge_cluster {
  description = "nsxt edge cluster"
  default = "islands-edge"
}

variable nsxt_t0 {
  description = "nsxt t0 router"
  default = "islands-t0"
}

variable nsxt_overlay_transport_zone {
  description = "Overlay Transport Zone"
  default = "nest-overlay-vds"
}

variable nsxt_dhcp_servers {
  description = "DHCP Server Addresses"
  default = ["172.29.0.1/32"]
}

variable nsxt_segment_subnet {
  description = "Segment CIDR"
  default = "10.1.110.1/24"
}

variable nsxt_segment_dhcp_subnets {
  description = "Segment DHCP CIDR"
  default = ["10.1.110.0/24"]
}

variable "kube_api_ip" {
  description = "IP for Kube API Load Balancer"
  default     = "10.0.170.3"
}


# variable "ca_name" {
#   description = "CA Authority"
#   default     = "honolulu-issuing-CA"
# }

variable "ssh_private_key" {
  description = "File path for SSH Private Keys"
}

variable metallb_subnet {
  description = "MetalLB Subtnet"
  default = "10.1.110.0/24"
}

variable metallb_enabled {
  description = "MetalLB Flag"
  default = false
}

variable ingress_nginx_enabled {
  default = false
}

variable ingress_ip {
  default = "192.168.251.125"
}

variable cloudflare_api_token {
  description = "base64 global cloudflare api token"
}

variable powerdns_api_token {
  description = "powerdns api token"
}