provider "vsphere" {
  user                 = var.vsphere_user
  password             = var.vsphere_password
  vsphere_server       = var.vsphere_vcenter
  allow_unverified_ssl = true
}

data "vsphere_datacenter" "dc" {
  name = var.vsphere_datacenter
}

data "vsphere_compute_cluster" "cluster" {
  name              = var.vsphere_vm_compute_cluster
  datacenter_id     = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore" {
  name          = var.vsphere_datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

# data "vsphere_resource_pool" "pool" {
#   name          = var.vsphere_resource_pool
#   datacenter_id = data.vsphere_datacenter.dc.id
# }

data "vsphere_network" "network" {
  name          = var.vsphere_network
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_content_library" "library" {
  name = "templates"
}

data "vsphere_content_library_item" "item" {
  name       = var.vsphere_virtual_machine_template
  library_id = data.vsphere_content_library.library.id
  type       = "ovf"
}

# data "vsphere_virtual_machine" "template" {
#   name          = var.vsphere_virtual_machine_template
#   datacenter_id = data.vsphere_datacenter.dc.id
# }

# Create a folder for the Rancher Cluster
resource "vsphere_folder" "rancher_cluster" {
  type          = "vm"
  path          = "${var.vsphere_folder}/${var.cluster_name}"
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_virtual_machine" "rke2" {
  name             = "${var.rancher_hostname_master}-${count.index + 1}"
  count            = var.rancher_count_master
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id
  folder           = vsphere_folder.rancher_cluster.path
  num_cpus         = 3
  memory           = 4096
  # guest_id         = data.vsphere_virtual_machine.template.guest_id

  # scsi_type = data.vsphere_virtual_machine.template.scsi_type

  enable_disk_uuid = true
  firmware = "efi"
  efi_secure_boot_enabled = true

  network_interface {
    network_id = data.vsphere_network.network.id
  }

  disk {
    label            = "disk0"
    size             = 64
    thin_provisioned = true
    unit_number      = 0
  }
  disk {
    label            = "disk1"
    size             = 64
    thin_provisioned = true
    unit_number      = 1
  }

  clone {
    template_uuid = data.vsphere_content_library_item.item.id
  }
  
  extra_config = {
    "guestinfo.metadata"          = base64gzip(templatefile("cloud-init/metadata.json",
        { hostname = "${var.rancher_hostname_master}-${count.index + 1}",
          network = base64gzip(templatefile("cloud-init/network-config.yaml",{
          ip_address = var.rke2_info[count.index].ip}))
        }))
    "guestinfo.metadata.encoding" = "gzip+base64"
    "guestinfo.userdata"          = base64gzip(templatefile("cloud-init/cloud-config.yaml",
        { lead_address = var.rke2_info[0].ip,
          master_index = count.index,
          cluster_name = var.cluster_name,
          rke2_cluster_secret = var.rke2_cluster_secret,
          ingress_ip = var.ingress_ip,
          metallb_subnet = var.metallb_subnet,
          cloudflare_api_token = var.cloudflare_api_token,
          powerdns_api_token = var.powerdns_api_token,
          rancher_url = var.rancher_url
        }))
    "guestinfo.userdata.encoding" = "gzip+base64"
  }
  lifecycle {
    ignore_changes  = [extra_config,clone,disk]
  }
}